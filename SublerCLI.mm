//
//  SublerCLI.mm
//  Subler
//
//  Copyright 2009-2019 Damiano Galassi. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "MP42File.h"
#import "MP42FileImporter.h"
#import "MP42MediaFormat.h"

#import "NSString+MP42Additions.h"

void print_help() {
    printf("usage:\n");

    printf("\t -dest <destination file> \n");
    printf("\t -dest options:\n");

    printf("\t\t -chapters <chapters file> \n");
    printf("\t\t -chapterspreview Create chapters preview images \n");
    printf("\t\t -remove Remove existing subtitles \n");
    printf("\t\t -optimize Optimize \n");
    printf("\t\t -metadata {Tag Name:Tag Value} \n");
    printf("\t\t -removemetadata remove all the tags \n");
    printf("\t\t -organizegroups enable tracks and create altenate groups in the iTunes friendly way\n");
    printf("\t\t -vprofile sets the video profile to any of <baseline, main, [high]>\n");
    printf("\t\t -vlevel sets the video level <21, 31, [41]>\n");
    printf("\n");

    printf("\t -source <source file> \n");
    printf("\t -source options:\n");
    printf("\t\t -listtracks For source file only, lists the tracks in the source movie. \n");
    printf("\t\t -listmetadata For source file only, lists the metadata in the source movie. \n");
    printf("\n");
    printf("\t\t -delay Delay in ms \n");
    printf("\t\t -height Height in pixel \n");
    printf("\t\t -language Track language (i.e. English) \n");
    printf("\t\t -downmix Downmix audio (mono, stereo, dolby, pl2) \n");
    printf("\t\t -64bitchunk 64bit file (only when -dest isn't an existing file) \n");
    printf("\n");

    printf("\t -help Print this help information \n");
    printf("\t -version Print version \n");
}

void print_version() {
    printf("\t\tversion 1.5.1\n");
}

MP42Image * _Nullable load_cover_art(NSString *filePath) {
    NSURL *fileURL = [NSURL fileURLWithPath:filePath];
    if (fileURL) {
        NSString *type;
        MP42Image *coverArt;
        if ([fileURL getResourceValue:&type forKey:NSURLTypeIdentifierKey error:NULL]) {
            if (UTTypeConformsTo((__bridge CFStringRef)type, (__bridge CFStringRef)@"public.jpeg")) {
                coverArt = [[MP42Image alloc] initWithData:[NSData dataWithContentsOfURL:fileURL] type:MP42_ART_JPEG];
            } else {
                NSImage *image = [[NSImage alloc] initWithContentsOfURL:fileURL];
                coverArt = [[MP42Image alloc] initWithImage:image];
            }
        }
        return coverArt;
    }
    return nil;
}

int main (int argc, const char * argv[]) {
    @autoreleasepool {

        NSString *destinationPath = nil;
        NSString *sourcePath = nil;
        NSString *chaptersPath = nil;
        NSString *metadata = nil;

        NSString *language = NULL;
        int delay = 0;
        unsigned int height = 0;
        BOOL removeExisting = false;
        BOOL chapterPreview = false;
        BOOL modified = false;
        BOOL optimize = false;

        BOOL listtracks = false;
        BOOL listmetadata = false;
        BOOL removemetadata = false;

        BOOL organizegroups = NO;

        BOOL _64bitchunk = NO;
        BOOL downmixAudio = NO;
        NSString *downmixArg = nil;
        MP42AudioMixdown downmixType = kMP42AudioMixdown_None;

        unsigned int videoprofile = 100; //high
        unsigned int videolevel = 41;    //4.1

        if (argc == 1) {
            print_help();
            exit(-1);
        }

        argv += 1;
        argc--;

        // Handle args
        while (argc > 0 && **argv == '-') {
            const char *args = &(*argv)[1];

            argc--;
            argv++;

            if ( ! strcmp ( args, "source" ) )
            {
                sourcePath = @(*argv++);
                argc--;
            }
            else if (( ! strcmp ( args, "dest" )) || ( ! strcmp ( args, "destination" )) )
            {
                destinationPath = @(*argv++);
                argc--;
            }
            else if ( ! strcmp ( args, "chapters" ) )
            {
                chaptersPath = @(*argv++);
                argc--;
            }
            else if ( ! strcmp ( args, "chapterspreview" ) )
            {
                chapterPreview = YES;
            }
            else if ( ! strcmp ( args, "metadata" ) )
            {
                metadata = @(*argv++);
                argc--;
            }
            else if ( ! strcmp ( args, "optimize" ) )
            {
                optimize = YES;
            }
            else if ( ! strcmp ( args, "downmix" ) )
            {
                downmixAudio = YES;
                downmixArg = @(*argv++);
                if(![downmixArg caseInsensitiveCompare:@"mono"]) downmixType = kMP42AudioMixdown_Mono;
                else if(![downmixArg caseInsensitiveCompare:@"stereo"]) downmixType = kMP42AudioMixdown_Stereo;
                else if(![downmixArg caseInsensitiveCompare:@"dolby"]) downmixType = kMP42AudioMixdown_Dolby;
                else if(![downmixArg caseInsensitiveCompare:@"pl2"]) downmixType = kMP42AudioMixdown_DolbyPlII;
                else {
                    printf( "Error: unsupported downmix type '%s'\n", optarg );
                    printf( "Valid downmix types are: 'mono', 'stereo', 'dolby' and 'pl2'\n" );
                    exit( -1 );
                }
                argc--;
            }
            else if ( ! strcmp ( args, "64bitchunk" ) )
            {
                _64bitchunk = YES;
            }
            else if ( ! strcmp ( args, "delay" ) )
            {
                delay = atoi(*argv++);
                argc--;
            }
            else if ( ! strcmp ( args, "height" ) )
            {
                height = atoi(*argv++);
                argc--;
            }
            else if ( ! strcmp ( args, "language" ) )
            {
                language = @(*argv++);
                argc--;
            }
            else if ( ! strcmp ( args, "remove" ) )
            {
                removeExisting = YES;
            }
            else if (( ! strcmp ( args, "version" )) || ( ! strcmp ( args, "v" )) )
            {
                print_version();
            }
            else if ( ! strcmp ( args, "help" ) )
            {
                print_help();
            }
            else if ( ! strcmp ( args, "listtracks" ) )
            {
                listtracks = YES;
            }
            else if ( ! strcmp ( args, "listmetadata" ) )
            {
                listmetadata = YES;
            }
            else if ( ! strcmp ( args, "removemetadata" ) )
            {
                removemetadata = YES;
            }
            else if ( ! strcmp ( args, "organizegroups" ) )
            {
                organizegroups = YES;
            }
            else if ( ! strcmp ( args, "vprofile" ) )
            {
                if (argc) {
                    const char *arg = *argv++;

                    if (!strcmp(arg, "baseline")) { videoprofile = 66; }
                    else if(!strcmp(arg, "main")) { videoprofile = 77; }
                    else if(!strcmp(arg, "high")) { videoprofile = 100; }
                    else {
                        printf( "Error: unsupported profile '%s'\n", arg );
                        printf( "Valid profiles are: 'baseline', 'main' and 'high'\n" );
                        exit( -1 );
                    }
                    argc--;
                }
                else {
                    print_help();
                    exit(-1);
                }
            }
            else if ( ! strcmp ( args, "vlevel" ) )
            {
                if (argc) {
                    videolevel = atoi(*argv++);
                    argc--;
                }
                else {
                    print_help();
                    exit(-1);
                }
            }
            else {
                printf("Invalid input parameter: %s\n", args );
                print_help();
                return 1;
            }
        }

        // Don't let the user mux a file to the file itself
        if ([sourcePath isEqualToString:destinationPath]) {
            printf("The destination path need to be different from the source path\n");
            exit(1);
        }

        // Lists tracks or metadatq, works with -source
        if (sourcePath && (listtracks || listmetadata)) {
            MP42File *mp4File = nil;

            if ([NSFileManager.defaultManager fileExistsAtPath:sourcePath]) {
                mp4File = [[MP42File alloc] initWithURL:[NSURL fileURLWithPath:sourcePath] error:NULL];
            }

            if (!mp4File) {
                printf("Error: %s\n", "the mp4 file couln't be opened.");
                return -1;
            }

            if (listtracks) {
                for (MP42Track* track in mp4File.tracks) {
                    printf("%s\n", track.description.UTF8String);
                }
            }

            if (listmetadata) {
                for (MP42MetadataItem *item in mp4File.metadata.items) {
                    printf("%s: %s\n", item.identifier.UTF8String, item.stringValue.UTF8String);
                }
            }

            return 0;
        }

        // Other options, works with -dest
        if ((sourcePath && [NSFileManager.defaultManager fileExistsAtPath:sourcePath]) ||
            organizegroups || chaptersPath || removeExisting || metadata || chapterPreview || removemetadata) {
            NSError *outError = nil;
            MP42File *mp4 = nil;
            NSMutableDictionary<NSString *, id> *attributes = [NSMutableDictionary dictionary];

            if ([NSFileManager.defaultManager fileExistsAtPath:destinationPath]) {
                mp4 = [[MP42File alloc] initWithURL:[NSURL fileURLWithPath:destinationPath] error:NULL];
            }
            else {
                mp4 = [[MP42File alloc] init];
            }

            if (!mp4) {
                printf("Error: %s\n", "the mp4 file couln't be opened.");
                return -1;
            }

            // Remove Metadata
            if (removemetadata) {
                MP42Metadata *metadata = mp4.metadata;
                [metadata removeMetadataItems:metadata.items];
                modified = YES;
            }

            // Remove subtitles tracks
            if (removeExisting) {
                NSArray<MP42Track *> *subtitlesTracks = [mp4 tracksWithMediaType:kMP42MediaType_Subtitle];
                if (subtitlesTracks.count) {
                    [mp4 removeTracks:subtitlesTracks];
                    modified = YES;
                }
            }

            // Import source
            if ((sourcePath && [NSFileManager.defaultManager fileExistsAtPath:sourcePath])) {
                NSURL *sourceURL = [NSURL fileURLWithPath:sourcePath];
                MP42FileImporter *fileImporter = [[MP42FileImporter alloc] initWithURL:sourceURL
                                                                                 error:&outError];

                for (MP42Track *track in fileImporter.tracks) {
                    if (language) {
                        track.language = language;
                    }
                    if (delay) {
                        track.startOffset = delay;
                    }
                    if (height && [track isMemberOfClass:[MP42SubtitleTrack class]]) {
                        [(MP42VideoTrack *)track setTrackHeight:height];
                    }

                    [mp4 addTrack:track];
                    modified = YES;
                }
            }

            // Import chapters
            if (chaptersPath) {
                MP42Track *oldChapterTrack = nil;
                MP42ChapterTrack *newChapterTrack = nil;

                MP42Track *track;
                for (track in mp4.tracks)
                    if ([track isMemberOfClass:[MP42ChapterTrack class]]) {
                        oldChapterTrack = track;
                        break;
                    }

                if (oldChapterTrack != nil) {
                    [mp4 removeTracks:@[oldChapterTrack]];
                    modified = YES;
                }

                newChapterTrack = [MP42ChapterTrack chapterTrackFromFile:[NSURL fileURLWithPath:chaptersPath]];

                if (newChapterTrack.chapterCount > 0) {
                    [mp4 addTrack:newChapterTrack];
                    modified = YES;
                }
            }

            // Select audio dowmix
            if (downmixAudio) {
                for (MP42AudioTrack *track in mp4.tracks) {
                    if (![track isKindOfClass:[MP42AudioTrack class]]) { continue; }
                    track.conversionSettings = [MP42AudioConversionSettings audioConversionWithBitRate:96
                                                                                               mixDown:downmixType
                                                                                                   drc:1];
                    modified = YES;
                }
            }

            // Set H.264 video profile
            for (MP42VideoTrack *track in mp4.tracks) {
                if ([track isKindOfClass:[MP42VideoTrack class]] && track.format == kMP42VideoCodecType_H264) {
                    track.newProfile = videoprofile;
                    track.newLevel = videolevel;
                    modified = YES;
                }
            }

            // Set metadata
            if (metadata) {
                NSString *searchString = metadata;
                NSString *regexCheck = @"(\\{[^:]*:[^\\}]*\\})*";

                // escaping the {, } and : charachters
                NSString *left_normal = @"{";
                NSString *right_normal = @"}";
                NSString *semicolon_normal = @":";

                NSString *left_escaped = @"&#123;";
                NSString *right_escaped = @"&#125;";
                NSString *semicolon_escaped = @"&#58;";

                if (searchString != nil && [searchString MP42_isMatchedByRegex:regexCheck]) {

                    NSString *regexSplitArgs = @"^\\{|\\}\\{|\\}$";
                    NSString *regexSplitValue = @"([^:]*):(.*)";

                    NSArray *argsArray = nil;
                    NSString *arg = nil;
                    NSString *key = nil;
                    NSString *value = nil;
                    argsArray = [searchString MP42_componentsSeparatedByRegex:regexSplitArgs];

                    for (arg in argsArray) {
                        key = [arg MP42_stringByMatching:regexSplitValue capture:1L];
                        value = [arg MP42_stringByMatching:regexSplitValue capture:2L];

                        key = [key stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                        value = [value stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

                        value = [value stringByReplacingOccurrencesOfString:left_escaped withString:left_normal];
                        value = [value stringByReplacingOccurrencesOfString:right_escaped withString:right_normal];
                        value = [value stringByReplacingOccurrencesOfString:semicolon_escaped withString:semicolon_normal];

                        if (key != nil && key.length > 0) {
                            NSArray<MP42MetadataItem *> *items = [mp4.metadata metadataItemsFilteredByIdentifier:key];
                            [mp4.metadata removeMetadataItems:items];

                            if (value != nil && value.length > 0) {
                                MP42MetadataItem *item;

                                if ([key isEqualToString:MP42MetadataKeyCoverArt]) {
                                    MP42Image *coverArt = load_cover_art(value);
                                    item = [MP42MetadataItem metadataItemWithIdentifier:key value:coverArt dataType:MP42MetadataItemDataTypeImage extendedLanguageTag:nil];
                                } else {
                                    item = [MP42MetadataItem metadataItemWithIdentifier:key value:value dataType:MP42MetadataItemDataTypeUnspecified extendedLanguageTag:nil];
                                }

                                [mp4.metadata addMetadataItem:item];
                            }
                            modified = YES;
                        }
                    }
                }
            }

            // Set chapters preview
            if (chapterPreview) {
                attributes[MP42GenerateChaptersPreviewTrack] = @YES;
                modified = YES;
            }

            // Organize groups
            if (organizegroups) {
                [mp4 organizeAlternateGroups];
                modified = YES;
            }

            BOOL success = NO;
            if (modified && mp4.hasFileRepresentation) {
                success = [mp4 updateMP4FileWithOptions:attributes error:&outError];
            } else if (destinationPath) {
                if (mp4.dataSize > 4100000000ll || _64bitchunk) {
                    attributes[MP4264BitData] = @YES;
                }

                success = [mp4 writeToUrl:[NSURL fileURLWithPath:destinationPath]
                                      options:attributes
                                        error:&outError];
            }

            if (!success) {
                if (outError) {
                    printf("Error: %s\n", outError.localizedDescription.UTF8String);
                }
                return -1;
            }

        }

        // -optimize option
        if (optimize) {
            NSError *outError;
            MP42File *mp4File;
            mp4File = [[MP42File alloc] initWithURL:[NSURL fileURLWithPath:destinationPath] error:&outError];
            if (!mp4File) {
                printf("Error: %s\n", outError.localizedDescription.UTF8String);
                return -1;
            }
            printf("Optimizing...\n");
            [mp4File optimize];
            printf("Done.\n");
        }
    }
    return 0;
}
